# Momenton code challenge
Employee hierarchy Api

## Framework
.NET CORE 2.2

## Project type
Web Api

## Note
* Service built on IDE Visual Studio 2017
* When run the solution, you should be able to see `Swagger` ui. 

## Assumption
* Employee can not have any more than 1 manager.
* Every EmployeeID is unique.
* Invalid ManagerId can be disregarded if not found from hierarchy
* Software Scalability was assumed for future work.

## HOW API WORKS
There are 3 apis available.
- HTTP PUT : /api/Employee
 => You can add `Employee`. example json parameter is provided in below.
	{
		"name": "Max",
		"managerId": null
	}
- HTTP GET : /api/Employee/id
=> You can get `Employee` information using this api
- HTTP GET : /api/Employee/hierarchy
=> You should be able to load hierarchy by providing `employeeId`. Api will respond information based on `Rank`.
   If you provide `null`, api will try build whole hierarchy of `employee`
   If you request specific `employeeId`, Api will respond every reporting employees hierarchy information by assuming given `employeeId` as the most parent.


## Author
* **Max Paik** - maxpaik79@gmail.com
