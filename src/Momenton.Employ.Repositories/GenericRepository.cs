﻿using Microsoft.EntityFrameworkCore;
using Momenton.Employee.Data;
using System.Threading.Tasks;

namespace Momenton.Employee.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private AppDbContext _context;
        private DbSet<T> _table;

        public GenericRepository(AppDbContext context)
        {
            _context = context;
            _table = _context.Set<T>();
        }

        public async Task<T[]> GetAsync()
        {
            return await _table.ToArrayAsync();
        }

        public async Task<T> GetForIdAsync(int id)
        {
            var model = await _table.FindAsync(id);
            return model;
        }

        public async Task<T> PutAsync(T obj)
        {
            var result = await _table.AddAsync(obj);
            await SaveAsync();
            return result.Entity;
        }

        public async void Update(T obj)
        {
            _table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            await SaveAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var exists = await _table.FindAsync(id);
            _table.Remove(exists);
            await SaveAsync();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
