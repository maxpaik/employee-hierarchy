﻿using System.Threading.Tasks;

namespace Momenton.Employee.Repositories
{
    public interface IGenericRepository<T> where T: class
    {
        Task<T[]> GetAsync();
        Task<T> GetForIdAsync(int id);
        Task<T> PutAsync(T obj);
        void Update(T obj);
        Task DeleteAsync(int id);
        Task SaveAsync();
    }
}
