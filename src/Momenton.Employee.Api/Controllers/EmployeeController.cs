﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Momenton.Employee.Api.Models;
using Momenton.Employee.Api.Services;
using Momenton.Employee.Data;
using Momenton.Employee.Repositories;

namespace Momenton.Employee.Api.Controllers
{
    /// <summary>
    /// Employee Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        public EmployeeController(IGenericRepository<EmployeeInfo> repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
            _service = new EmployeeService(repo, _mapper);
        }

        [HttpGet]
        [Route("hierarchy")]
        public async Task<JsonResult> GetHierachy(int? id)
        {
            try
            {
                var result = await _service.GetHierarchyAsync(id);
                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                return new JsonResult($@"Error occurred: {ex.Message}");
            }
            
        }

        [HttpGet("id")]
        public async Task<JsonResult> Get(int id)
        {
            try
            {
                var result = await _service.GetAsync(id);
                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                return new JsonResult($@"Error occurred: {ex.Message}");
            }
        }

        [HttpPut]
        public async Task<JsonResult> Put(EmployeeRequestModel employee)
        {
            try
            {
                var result = await _service.PutAsync(employee);
                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                return new JsonResult($@"Error occurred: {ex.Message}");
            }
        }

        private readonly IMapper _mapper;
        private readonly IGenericRepository<EmployeeInfo> _repo;
        private readonly EmployeeService _service;
    }
}
