﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Momenton.Employee.Data;

namespace Momenton.Employee.Api.Models
{
    /// <summary>
    /// Employee Model
    /// </summary>
    public class EmployeeModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// manager id
        /// </summary>
        public int? managerId { get; set; }
    }

    /// <summary>
    /// Employee Model
    /// </summary>
    public class EmployeeRequestModel
    {
        /// <summary>
        /// Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// manager id
        /// </summary>
        public int? managerId { get; set; }
    }

    public class EmployeeChartModel
    {
        /// <summary>
        /// Employee
        /// </summary>
        public EmployeeModel employee { get; set; }
        /// <summary>
        /// ReportingFrom
        /// </summary>
        public EmployeeModel[] reportingFrom { get; set; }
        /// <summary>
        /// Rank
        /// </summary>
        public int rank { get; set; }
    }
}
