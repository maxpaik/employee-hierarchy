﻿using AutoMapper;
using Momenton.Employee.Api.Models;
using Momenton.Employee.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Momenton.Employee.Api.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmployeeInfo, EmployeeModel>();
            CreateMap<EmployeeModel, EmployeeInfo>();
        }
    }
}
