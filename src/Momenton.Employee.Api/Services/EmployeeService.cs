﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Internal;
using Momenton.Employee.Api.Models;
using Momenton.Employee.Data;
using Momenton.Employee.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Momenton.Employee.Api.Services
{
    public class EmployeeService
    {
        private readonly IMapper _mapper;
        public EmployeeService(IGenericRepository<EmployeeInfo> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<EmployeeModel[]> GetAsync()
        {
            var result = await _repo.GetAsync();
            return result
                .Select(x => _mapper.Map<EmployeeInfo, EmployeeModel>(x))
                .ToArray();
        }

        public async Task<EmployeeModel> GetAsync(int id)
        {
            var result = await _repo.GetForIdAsync(id);
            return _mapper.Map<EmployeeInfo, EmployeeModel>(result);
        }

        public async Task<List<EmployeeChartModel>> GetHierarchyAsync(int? id = null)
        {
            var employees = await GetAsync();

            var groupedEmployees = employees
                .Select(x => new KeyValuePair<EmployeeModel, EmployeeModel[]>(x, employees.Where(s => s.managerId == x.id).ToArray()))
                .ToList();

            var results = new List<EmployeeChartModel>();

            void SortedDictionary(int currentRank, int? managerId)
            {
                KeyValuePair<EmployeeModel, EmployeeModel[]> node;
                if (managerId == null)
                    node = groupedEmployees.FirstOrDefault(x => x.Key.managerId == null);
                else
                {
                    if (!groupedEmployees.Any(x => x.Key.id == managerId))
                        throw new InvalidOperationException($@"Employee data not found(ManagerId:{managerId ?? -1})");

                    node = groupedEmployees.FirstOrDefault(x => x.Key.id == managerId);
                }

                groupedEmployees.Remove(node);

                results.Add(new EmployeeChartModel
                {
                    employee = node.Key,
                    reportingFrom = node.Value,
                    rank = currentRank
                });

                if (node.Value != null && node.Value.Any())
                {
                    var newRank = currentRank + 1;
                    node.Value.ToList().ForEach(x => SortedDictionary(newRank, x.id));
                }

            }

            SortedDictionary(1, id);

            return results.OrderBy(x => x.rank).ToList();
        }

        public async Task<EmployeeModel> PutAsync(EmployeeRequestModel obj)
        {
            if (obj == null)
                throw new InvalidOperationException("Data not found");

            var result = await _repo.PutAsync(new EmployeeInfo
            {
                ManagerId = obj.managerId,
                Name = obj.name
            });
            return _mapper.Map<EmployeeInfo, EmployeeModel>(result);
        }

        public void Update(EmployeeInfo obj)
        {
            _repo.Update(obj);
        }

        private readonly IGenericRepository<EmployeeInfo> _repo;
    }
}
