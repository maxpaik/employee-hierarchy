﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Momenton.Employee.Api.Models;
using Momenton.Employee.Api.Profiles;
using Momenton.Employee.Api.Services;
using Momenton.Employee.Data;
using Momenton.Employee.Repositories;
using Swashbuckle.AspNetCore.Swagger;

namespace Momenton.Employee.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IGenericRepository<EmployeeInfo>, GenericRepository<EmployeeInfo>>();

            
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddCors(options =>
            {
                options.AddPolicy(_myOrigins,
                builder =>
                {
                    builder.WithOrigins("*")
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_version, new Info { Title = _title, Version = _version });
                c.DescribeAllParametersInCamelCase();
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AppDbContext dbContext)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            dbContext.Database.EnsureCreated();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $@"{_title} {_version}");
                c.DocumentTitle = _title;
                c.RoutePrefix = string.Empty;
            });
            
            app.UseCors(_myOrigins);
            app.UseHttpsRedirection();
            app.UseMvc();
        }

        private readonly string _myOrigins = "_momentonEmployeeApiCorsOrigin";
        private readonly string _title = "Momenton Employee Api";
        private readonly string _version = "v1";
    }
}
