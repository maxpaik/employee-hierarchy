﻿using Microsoft.EntityFrameworkCore;

namespace Momenton.Employee.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
            _options = options;
        }

        public virtual DbSet<EmployeeInfo> EmployeeInfo { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<EmployeeInfo>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired()
                    .ValueGeneratedOnAdd()
                    .IsUnicode(false);
            });
        }

        private DbContextOptions _options;
    }
}
