using AutoMapper;
using Momenton.Employee.Api.Models;
using Momenton.Employee.Api.Profiles;
using Momenton.Employee.Api.Services;
using Momenton.Employee.Data;
using Momenton.Employee.Repositories;
using Moq;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            _mapper = mappingConfig.CreateMapper();

            _moqRepo = new Mock<IGenericRepository<EmployeeInfo>>();
            _moqRepo.Setup(m => m.GetAsync())
                .Returns(Task.FromResult(_sampleEmployeeSetup));

            _service = new EmployeeService(_moqRepo.Object, _mapper);
        }

        [Test]
        public void BasicRequirementTest()
        {
            var result = _service.GetHierarchyAsync(null).GetAwaiter().GetResult();

            result.ShouldNotBe(null);
            result.Count.ShouldBe(6);

            
            result.Where(x => x.rank == 1).Count().ShouldBe(1);
            result.First(x => x.rank == 1).employee.id.ShouldBe(150);
            result.First(x => x.rank == 1).employee.name.ShouldBe("Jamie");
            result.First(x => x.rank == 1).reportingFrom.Count().ShouldBe(2);

            result.Where(x => x.rank == 2).Count().ShouldBe(2);
            result.Where(x => x.rank == 2).Select(e => e.employee.name).ShouldContain("Alan");
            result.Where(x => x.rank == 2).Select(e => e.employee.name).ShouldContain("Steve");

            result.Where(x => x.rank == 3).Count().ShouldBe(3);
            result.Where(x => x.rank == 3).Select(e => e.employee.name).ShouldContain("Martin");
            result.Where(x => x.rank == 3).Select(e => e.employee.name).ShouldContain("Alex");
            result.Where(x => x.rank == 3).Select(e => e.employee.name).ShouldContain("David");

            result.Where(x => x.rank == 4).Count().ShouldBe(0);
        }

        [Test]
        public void PartialHierarchyCreationTest()
        {
            var result = _service.GetHierarchyAsync(100).GetAwaiter().GetResult();

            result.ShouldNotBe(null);
            result.Count.ShouldBe(3);


            result.Where(x => x.rank == 1).Count().ShouldBe(1);
            result.First(x => x.rank == 1).employee.id.ShouldBe(100);
            result.First(x => x.rank == 1).employee.name.ShouldBe("Alan");
            result.First(x => x.rank == 1).reportingFrom.Count().ShouldBe(2);

            result.Where(x => x.rank == 2).Count().ShouldBe(2);
            result.Where(x => x.rank == 2).Select(e => e.employee.name).ShouldContain("Martin");
            result.Where(x => x.rank == 2).Select(e => e.employee.name).ShouldContain("Alex");

            result.Max(x => x.rank).ShouldBe(2);
        }


        private EmployeeInfo[] _sampleEmployeeSetup
        {
            get
            {
                return new List<EmployeeInfo>
                {
                    new EmployeeInfo { Id = 100, ManagerId = 150, Name = "Alan" },
                    new EmployeeInfo { Id = 220, ManagerId = 100, Name = "Martin" },
                    new EmployeeInfo { Id = 150, ManagerId = null, Name = "Jamie" },
                    new EmployeeInfo { Id = 275, ManagerId = 100, Name = "Alex" },
                    new EmployeeInfo { Id = 400, ManagerId = 150, Name = "Steve" },
                    new EmployeeInfo { Id = 190, ManagerId = 400, Name = "David" }
                }.ToArray();
            }
        }



        private Mock<IGenericRepository<EmployeeInfo>> _moqRepo;
        private IMapper _mapper;
        private EmployeeService _service;
    }
}